# -*- coding: utf-8 -*-
# Generated by Django 1.10.1 on 2016-10-11 12:58
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('general', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='homecontent',
            name='first_training_day',
            field=models.CharField(default='', max_length=255),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='homecontent',
            name='first_training_lat',
            field=models.FloatField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='homecontent',
            name='first_training_lng',
            field=models.FloatField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='homecontent',
            name='first_training_time',
            field=models.CharField(default='', max_length=255),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='homecontent',
            name='second_training_day',
            field=models.CharField(default=1, max_length=255),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='homecontent',
            name='second_training_lat',
            field=models.FloatField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='homecontent',
            name='second_training_lng',
            field=models.FloatField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='homecontent',
            name='second_training_time',
            field=models.CharField(default=1, max_length=255),
            preserve_default=False,
        ),
    ]
