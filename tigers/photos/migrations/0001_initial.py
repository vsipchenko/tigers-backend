# -*- coding: utf-8 -*-
# Generated by Django 1.10.1 on 2016-09-27 14:22
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion
import tigers.general.utils


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Album',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255)),
            ],
        ),
        migrations.CreateModel(
            name='Photo',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('photo', models.ImageField(upload_to=tigers.general.utils.generate_filename)),
                ('album', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='photos.Album')),
            ],
        ),
    ]
