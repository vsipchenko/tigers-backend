from django.db import models
from sorl.thumbnail import get_thumbnail

from tigers.general.utils import generate_filename


class Album(models.Model):
    name = models.CharField(max_length=255)

    def __str__(self):
        return str(self.name)


class Photo(models.Model):
    album = models.ForeignKey(Album)
    photo = models.ImageField(upload_to=generate_filename)


class HomePageSliders(models.Model):
    image = models.ImageField(upload_to=generate_filename)

    def get_cropped_image(self, *args, **kwargs):
        return get_thumbnail(self.image, '1286x500', crop='center')
