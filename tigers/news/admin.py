from django.contrib import admin

from tigers.news.models import News

admin.site.register(News)