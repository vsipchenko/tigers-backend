from datetime import datetime

from django.db import models

from tigers.general.utils import generate_filename
from sorl.thumbnail import get_thumbnail


class News(models.Model):
    title = models.CharField(max_length=255)
    text = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
    image = models.ImageField(upload_to=generate_filename)
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return str(self.title)

    def get_pretty_date(self):
        return datetime.strftime(self.created_at, "%d/%m/%y %H:%M")

    def get_cropped_image(self, *args, **kwargs):
        return get_thumbnail(self.image, '300x300', crop='center')
