from rest_framework import serializers
#
from tigers.general.models import HomeContent, Contacts, Training
from tigers.news.models import News
from tigers.photos.models import Album, Photo, HomePageSliders


class HomeContentSerializer(serializers.ModelSerializer):
    class Meta:
        model = HomeContent
        fields = ['welcome_text', 'welcome_title', 'team_introduction_text',
                  'team_introduction_title', 'about_ultimate_short_note',
                  ]


class TrainingSerializer(serializers.ModelSerializer):
    class Meta:
        model = Training
        fields = ['day', 'time', 'location', 'price', 'latitude_coordinates',
                  'length_coordinates']


class ContactsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Contacts
        fields = ['first_phone', 'second_phone', 'email', 'vk_link', 'fb_link',
                  'other']


class NewsSerializer(serializers.ModelSerializer):
    cropped_image_url = serializers.ImageField(source='get_cropped_image',read_only=True)
    pretty_date = serializers.CharField(source='get_pretty_date', read_only=True)

    class Meta:
        model = News
        fields = ['title', 'text', 'created_at', 'pretty_date', 'cropped_image_url']


class AlbumSerializer(serializers.HyperlinkedModelSerializer):
    url = serializers.HyperlinkedIdentityField(
        view_name='api:albums-detail'
    )

    class Meta:
        model = Album
        fields = [
            'id',
            'name',
            'url',
        ]


class PhotoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Photo
        fields = ['photo', 'album']


class HomePageSliderSerializer(serializers.ModelSerializer):
    cropped_image_url = serializers.ImageField(source='get_cropped_image',
                                               read_only=True)

    class Meta:
        model = HomePageSliders
        fields = ['cropped_image_url']
