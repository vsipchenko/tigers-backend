from functools import partial

from rest_framework.response import Response
from rest_framework.decorators import api_view, detail_route
from rest_framework.reverse import reverse
from rest_framework import generics
from rest_framework.viewsets import ReadOnlyModelViewSet

from tigers.api.serializers import HomeContentSerializer, \
    ContactsSerializer, NewsSerializer, AlbumSerializer, PhotoSerializer, \
    HomePageSliderSerializer, TrainingSerializer
from tigers.general.models import HomeContent, Contacts, Training
from tigers.news.models import News
from tigers.photos.models import HomePageSliders, Album


@api_view()
def api_root(request, format=None):
    r = partial(reverse, request=request, format=format)
    return Response([
        {
            'api': [
                {'contacts': r('api:contacts')},
                {'home': r('api:home')},
                {'news': r('api:news-list')},
                {'albums': r('api:albums-list')},
                {'sliders': r('api:slider-list')},
                {'training': r('api:training-list')}

            ]
        }
    ])


@api_view()
def home_content_view(request):
    obj = HomeContent.objects.get()
    serializer = HomeContentSerializer(obj)
    return Response(serializer.data)


@api_view()
def homepage_sliders_view(request):
    sliders = HomePageSliders.objects.all()
    serializer = HomePageSliderSerializer(sliders, many=True,
                                          context={'request': request})
    return Response(serializer.data)


@api_view()
def training_view(request):
    training = Training.objects.all()
    serializer = TrainingSerializer(training, many=True,
                                          context={'request': request})
    return Response(serializer.data)


@api_view()
def contacts_view(request):
    obj = Contacts.objects.get()
    serializer = ContactsSerializer(obj)
    return Response(serializer.data)


class NewsListView(generics.ListAPIView):
    queryset = News.objects.filter(is_active=True)
    serializer_class = NewsSerializer


class AlbumViewSet(ReadOnlyModelViewSet):
    queryset = Album.objects.all()
    serializer_class = AlbumSerializer

    @detail_route(methods=['get'])
    def photos(self, request, pk):
        album = Album.objects.get(id=pk)
        photos = album.photo_set.all()
        serializer = PhotoSerializer(photos, context={'request': request},
                                     many=True)
        return Response(serializer.data)
